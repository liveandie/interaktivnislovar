export let itemsData = new Map([
  ['KosareIzbiranje', [
    'ananas',
    'banane',
    'borovnice',
    'breskve',
    'hruske',
    'jabolka',
    'jagode',
    'kivi',
    'limone',
    'lubenica',
    'maline',
    'marelice',
    'pomarance',
    'slive',
    'spinaca',
    'por',
    'paprika',
    'paradiznik',
    'korencek',
    'koruza',
    'krompir',
    'kumara',
    'cvetaca',
    'fizol',
    'grah',
    'bucka',
    'cebula',
    'cesen',
    'cesnje'
  ]]
])

export let platePositions = [
  { x: 232, y: 450 },
  { x: 656, y: 450 },
  { x: 1074, y: 450 }
]
