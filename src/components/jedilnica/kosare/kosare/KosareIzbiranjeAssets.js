import ananas from '@/assets/jedilnica/kosare/kosare/ananas.svg'
import banane from '@/assets/jedilnica/kosare/kosare/banane.svg'
import borovnice from '@/assets/jedilnica/kosare/kosare/borovnice.svg'
import breskve from '@/assets/jedilnica/kosare/kosare/breskve.svg'
import hruske from '@/assets/jedilnica/kosare/kosare/hruske.svg'
import jabolka from '@/assets/jedilnica/kosare/kosare/jabolka.svg'
import jagode from '@/assets/jedilnica/kosare/kosare/jagode.svg'
import kivi from '@/assets/jedilnica/kosare/kosare/kivi.svg'
import limone from '@/assets/jedilnica/kosare/kosare/limone.svg'
import lubenica from '@/assets/jedilnica/kosare/kosare/lubenica.svg'
import maline from '@/assets/jedilnica/kosare/kosare/maline.svg'
import marelice from '@/assets/jedilnica/kosare/kosare/marelice.svg'
import pomarance from '@/assets/jedilnica/kosare/kosare/pomarance.svg'
import slive from '@/assets/jedilnica/kosare/kosare/slive.svg'
import spinaca from '@/assets/jedilnica/kosare/kosare/spinaca.svg'
import por from '@/assets/jedilnica/kosare/kosare/por.svg'
import paprika from '@/assets/jedilnica/kosare/kosare/paprika.svg'
import paradiznik from '@/assets/jedilnica/kosare/kosare/paradiznik.svg'
import korencek from '@/assets/jedilnica/kosare/kosare/korencek.svg'
import koruza from '@/assets/jedilnica/kosare/kosare/koruza.svg'
import krompir from '@/assets/jedilnica/kosare/kosare/krompir.svg'
import kumara from '@/assets/jedilnica/kosare/kosare/kumara.svg'
import cvetaca from '@/assets/jedilnica/kosare/kosare/cvetaca.svg'
import fizol from '@/assets/jedilnica/kosare/kosare/fizol.svg'
import grah from '@/assets/jedilnica/kosare/kosare/grah.svg'
import bucka from '@/assets/jedilnica/kosare/kosare/bucka.svg'
import cebula from '@/assets/jedilnica/kosare/kosare/cebula.svg'
import cesen from '@/assets/jedilnica/kosare/kosare/cesen.svg'
import cesnje from '@/assets/jedilnica/kosare/kosare/cesnje.svg'

export default {
  ananas,
  banane,
  borovnice,
  breskve,
  hruske,
  jabolka,
  jagode,
  kivi,
  limone,
  lubenica,
  maline,
  marelice,
  pomarance,
  slive,
  spinaca,
  por,
  paprika,
  paradiznik,
  korencek,
  koruza,
  krompir,
  kumara,
  cvetaca,
  fizol,
  grah,
  bucka,
  cebula,
  cesen,
  cesnje
}
