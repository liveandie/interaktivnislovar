import ananas from '@/assets/jedilnica/kosare/sadnaKupa/ananas.svg'
import banana from '@/assets/jedilnica/kosare/sadnaKupa/banana.svg'
import borovnica from '@/assets/jedilnica/kosare/sadnaKupa/borovnica.svg'
import breskev from '@/assets/jedilnica/kosare/sadnaKupa/breskev.svg'
import cesnja from '@/assets/jedilnica/kosare/sadnaKupa/cesnja.svg'
import hruska from '@/assets/jedilnica/kosare/sadnaKupa/hruska.svg'
import jabolko from '@/assets/jedilnica/kosare/sadnaKupa/jabolko.svg'
import jagoda from '@/assets/jedilnica/kosare/sadnaKupa/jagoda.svg'
import kivi from '@/assets/jedilnica/kosare/sadnaKupa/kivi.svg'
import kozarec from '@/assets/jedilnica/kosare/sadnaKupa/kozarec.svg'
import limona from '@/assets/jedilnica/kosare/sadnaKupa/limona.svg'
import lubenica from '@/assets/jedilnica/kosare/sadnaKupa/lubenica.svg'
import malina from '@/assets/jedilnica/kosare/sadnaKupa/malina.svg'
import marelica from '@/assets/jedilnica/kosare/sadnaKupa/marelica.svg'
import pomaranca from '@/assets/jedilnica/kosare/sadnaKupa/pomaranca.svg'
import sliva from '@/assets/jedilnica/kosare/sadnaKupa/sliva.svg'

export default {
  ananas,
  banana,
  borovnica,
  breskev,
  cesnja,
  hruska,
  jabolko,
  jagoda,
  kivi,
  kozarec,
  limona,
  lubenica,
  malina,
  marelica,
  pomaranca,
  sliva
}
