export let targetsData = new Map([
  ['SadnaKupaSestavi', {
    kozarec: {glyph: 'kozarec', matchId: 'kozarec', pos: {x: 294, y: 490}}
  }]
])

export let itemsData = new Map([
  ['SadnaKupaSestavi', {
    ananas: {glyph: 'ananas', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 644, y: 325}},
    banana: {glyph: 'banana', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 1152, y: 433}},
    borovnica: {glyph: 'borovnica', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 24, y: 456}},
    breskev: {glyph: 'breskev', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 774, y: 524}},
    cesnja: {glyph: 'cesnja', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 756, y: 434}},
    hruska: {glyph: 'hruska', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 498, y: 570}},
    jabolko: {glyph: 'jabolko', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 888, y: 578}},
    jagoda: {glyph: 'jagoda', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 1025, y: 544}},
    kivi: {glyph: 'kivi', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 38, y: 616}},
    limona: {glyph: 'limona', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 124, y: 500}},
    lubenica: {glyph: 'lubenica', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 1130, y: 596}},
    malina: {glyph: 'malina', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 202, y: 615}},
    marelica: {glyph: 'marelica', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 14, y: 533}},
    pomaranca: {glyph: 'pomaranca', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 660, y: 600}},
    sliva: {glyph: 'sliva', animateTo: 'kozarec', matchId: 'kozarec', pos: {x: 546, y: 500}}
  }]
])

export let inputsData = new Map([
  ['SadnaKupaSestavi', {
    ananas: {openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'ananas', pos: {x: 188, y: 198}},
    banana: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'banane', pos: {x: 188, y: 198}},
    borovnica: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'borovnice', pos: {x: 188, y: 198}},
    breskev: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'breskve', pos: {x: 188, y: 198}},
    cesnja: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'češnje', pos: {x: 188, y: 198}},
    hruska: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'hruške', pos: {x: 188, y: 198}},
    jabolko: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'jabolka', pos: {x: 188, y: 198}},
    jagoda: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'jagode', pos: {x: 188, y: 198}},
    kivi: {openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'kivi', pos: {x: 188, y: 198}},
    limona: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'limone', pos: {x: 188, y: 198}},
    lubenica: {openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'lubenica', pos: {x: 188, y: 198}},
    malina: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'maline', pos: {x: 188, y: 198}},
    marelica: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'marelice', pos: {x: 188, y: 198}},
    pomaranca: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'pomaranče', pos: {x: 188, y: 198}},
    sliva: {openDirection: 'right', arrowDirection: 'left', preText: 'To so', solution: 'slive', pos: {x: 188, y: 198}}
  }]])

export let linesData = new Map([
  ['SadnaKupaSestavi', {
    ana1: 'Kaj je v tvoji sadni kupi?',
    jakob1: 'V moji sadni kupi so',
    jagode: 'jagode',
    borovnice: 'borovnice',
    banane: 'banane.',
    poskusi: 'Zdaj pa poskusi še ti.',
    ana2: 'Kaj je v tvoji sadni kupi?',
    doberTek: 'Dober tek.'
  }]
])
