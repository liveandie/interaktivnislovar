export let targetsData = new Map([
  ['JedilniPripomocki', {
    kroznik: {glyph: 'kroznik', matchId: 'kroznik', pos: {x: 88, y: 282}},
    kozarec: {glyph: 'kozarec', matchId: 'kozarec', pos: {x: 414, y: 256}},
    skodelica: {glyph: 'skodelica', matchId: 'skodelica', pos: {x: 550, y: 270}},
    zlica: {glyph: 'zlica', matchId: 'zlica', pos: {x: 455, y: 428}},
    noz: {glyph: 'noz', matchId: 'noz', pos: {x: 262, y: 394}},
    vilice: {glyph: 'vilice', matchId: 'vilice', pos: {x: 100, y: 434}},
    pladenj: {glyph: 'pladenj', matchId: 'pladenj', pos: {x: 868, y: 323}},
    prticek: {glyph: 'prticek', matchId: 'prticek', pos: {x: 616, y: 340}}
  }]
])

export let labelsData = new Map([
  ['JedilniPripomockiPovezovanje', {
    kroznik: {animateTo: 'kroznik', matchId: 'kroznik', text: 'To je krožnik.'},
    kozarec: {animateTo: 'kozarec', matchId: 'kozarec', text: 'To je kozarec.'},
    skodelica: {animateTo: 'skodelica', matchId: 'skodelica', text: 'To je skodelica.'},
    zlica: {animateTo: 'zlica', matchId: 'zlica', text: 'To je žlica.'},
    noz: {animateTo: 'noz', matchId: 'noz', text: 'To je nož.'},
    vilice: {animateTo: 'vilice', matchId: 'vilice', text: 'To so vilice.'},
    pladenj: {animateTo: 'pladenj', matchId: 'pladenj', text: 'To je pladenj.'},
    prticek: {animateTo: 'prticek', matchId: 'prticek', text: 'To je prtiček.'}
  }]
])

export let inputsData = new Map([
  ['JedilniPripomockiVpisovanje', {
    kroznik: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'krožnik', pos: {x: 188, y: 198}},
    kozarec: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'kozarec', pos: {x: 424, y: 140}},
    skodelica: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'skodelica', pos: {x: 554, y: 198}},
    zlica: {openDirection: 'right', arrowDirection: 'top', preText: 'To je', solution: 'žlica', pos: {x: 512, y: 480}},
    noz: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'nož', pos: {x: 316, y: 364}},
    vilice: {openDirection: 'right', arrowDirection: 'top', preText: 'To so', solution: 'vilice', pos: {x: 152, y: 480}},
    pladenj: {openDirection: 'left', arrowDirection: 'top', preText: 'To je', solution: 'pladenj', pos: {x: 1018, y: 480}},
    prticek: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'prtiček', pos: {x: 704, y: 280}}
  }]])
