import kroznik from '@/assets/jedilnica/kuhinja/pripomocki/kroznik.svg'
import kozarec from '@/assets/jedilnica/kuhinja/pripomocki/kozarec.svg'
import skodelica from '@/assets/jedilnica/kuhinja/pripomocki/skodelica.svg'
import zlica from '@/assets/jedilnica/kuhinja/pripomocki/zlica.svg'
import noz from '@/assets/jedilnica/kuhinja/pripomocki/noz.svg'
import vilice from '@/assets/jedilnica/kuhinja/pripomocki/vilice.svg'
import pladenj from '@/assets/jedilnica/kuhinja/pripomocki/pladenj.svg'
import prticek from '@/assets/jedilnica/kuhinja/pripomocki/prticek.svg'

export default {
  kroznik,
  kozarec,
  skodelica,
  zlica,
  noz,
  vilice,
  pladenj,
  prticek
}
