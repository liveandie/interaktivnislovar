import caj from '@/assets/jedilnica/jedilnica/malica/caj.svg'
import jogurt from '@/assets/jedilnica/jedilnica/malica/jogurt.svg'
import kakav from '@/assets/jedilnica/jedilnica/malica/kakav.svg'
import kosmici from '@/assets/jedilnica/jedilnica/malica/kosmici.svg'
import kruh from '@/assets/jedilnica/jedilnica/malica/kruh.svg'
import makovka from '@/assets/jedilnica/jedilnica/malica/makovka.svg'
import marmelada from '@/assets/jedilnica/jedilnica/malica/marmelada.svg'
import maslo from '@/assets/jedilnica/jedilnica/malica/maslo.svg'
import med from '@/assets/jedilnica/jedilnica/malica/med.svg'
import mleko from '@/assets/jedilnica/jedilnica/malica/mleko.svg'
import pasteta from '@/assets/jedilnica/jedilnica/malica/pasteta.svg'
import salama from '@/assets/jedilnica/jedilnica/malica/salama.svg'
import sir from '@/assets/jedilnica/jedilnica/malica/sir.svg'
import zemlja from '@/assets/jedilnica/jedilnica/malica/zemlja.svg'

export default {
  caj,
  jogurt,
  kakav,
  kosmici,
  kruh,
  makovka,
  marmelada,
  maslo,
  med,
  mleko,
  pasteta,
  salama,
  sir,
  zemlja
}
