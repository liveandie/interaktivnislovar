export let targetsData = new Map([
  ['MalicaPovezovanje', {
    kruh1: {glyph: 'kruh', matchId: 'kruh', pos: {x: 258, y: 141}},
    marmelada: {glyph: 'marmelada', matchId: 'marmelada', pos: {x: 408, y: 127}},
    caj: {glyph: 'caj', matchId: 'caj', pos: {x: 544, y: 134}},
    zemlja: {glyph: 'zemlja', matchId: 'zemlja', pos: {x: 182, y: 254}},
    salama: {glyph: 'salama', matchId: 'salama', pos: {x: 320, y: 258}},
    sir: {glyph: 'sir', matchId: 'sir', pos: {x: 466, y: 252}},
    jogurt: {glyph: 'jogurt', matchId: 'jogurt', pos: {x: 612, y: 238}},
    makovka: {glyph: 'makovka', matchId: 'makovka', pos: {x: 244, y: 348}},
    pasteta: {glyph: 'pasteta', matchId: 'pasteta', pos: {x: 418, y: 357}},
    kakav: {glyph: 'kakav', matchId: 'kakav', pos: {x: 534, y: 342}},
    kosmici: {glyph: 'kosmici', matchId: 'kosmici', pos: {x: 310, y: 444}},
    mleko1: {glyph: 'mleko', matchId: 'mleko', pos: {x: 473, y: 440}},
    kruh2: {glyph: 'kruh', matchId: 'kruh', pos: {x: 204, y: 555}},
    maslo: {glyph: 'maslo', matchId: 'maslo', pos: {x: 326, y: 560}},
    med: {glyph: 'med', matchId: 'med', pos: {x: 475, y: 542}},
    mleko2: {glyph: 'mleko', matchId: 'mleko', pos: {x: 588, y: 542}}
  }]
])

export let labelsData = new Map([
  ['MalicaPovezovanje', {
    caj: {animateTo: 'caj', matchId: 'caj', text: 'To je čaj.'},
    jogurt: {animateTo: 'jogurt', matchId: 'jogurt', text: 'To je jogurt.'},
    kakav: {animateTo: 'kakav', matchId: 'kakav', text: 'To je kakav.'},
    kosmici: {animateTo: 'kosmici', matchId: 'kosmici', text: 'To so kosmiči.'},
    kruh: {animateTo: 'kruh1', matchId: 'kruh', text: 'To je kruh.'},
    makovka: {animateTo: 'makovka', matchId: 'makovka', text: 'To je makovka.'},
    marmelada: {animateTo: 'marmelada', matchId: 'marmelada', text: 'To je marmelada.'},
    maslo: {animateTo: 'maslo', matchId: 'maslo', text: 'To je maslo.'},
    med: {animateTo: 'med', matchId: 'med', text: 'To je med.'},
    mleko: {animateTo: 'mleko1', matchId: 'mleko', text: 'To je mleko.'},
    pasteta: {animateTo: 'pasteta', matchId: 'pasteta', text: 'To je pašteta.'},
    salama: {animateTo: 'salama', matchId: 'salama', text: 'To je salama.'},
    sir: {animateTo: 'sir', matchId: 'sir', text: 'To je sir.'},
    zemlja: {animateTo: 'zemlja', matchId: 'zemlja', text: 'To je žemlja.'}
  }]
])
