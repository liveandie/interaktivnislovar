export let targetsData = new Map([
  ['ZelenjavaPovezovanje', {
    bucka: {matchId: 'bucka', pos: {x: 640, y: 43}},
    cebula: {matchId: 'cebula', pos: {x: 806, y: 152}},
    cesen: {matchId: 'cesen', pos: {x: 918, y: 295}},
    cvetaca: {matchId: 'cvetaca', pos: {x: 414, y: 50}},
    fizol: {matchId: 'fizol', pos: {x: 942, y: 187}},
    grah: {matchId: 'grah', pos: {x: 55, y: 238}},
    korencek: {matchId: 'korencek', pos: {x: 226, y: 36}},
    koruza: {matchId: 'koruza', pos: {x: 916, y: 63}},
    krompir: {matchId: 'krompir', pos: {x: 458, y: 314}},
    kumara: {matchId: 'kumara', pos: {x: 182, y: 372}},
    paprika: {matchId: 'paprika', pos: {x: 125, y: 52}},
    paradiznik: {matchId: 'paradiznik', pos: {x: 727, y: 287}},
    por: {matchId: 'por', pos: {x: 1113, y: 128}},
    spinaca: {matchId: 'spinaca', pos: {x: 340, y: 235}}
  }]
])

export let labelsData = new Map([
  ['ZelenjavaPovezovanje', {
    bucka: {animateTo: 'bucka', matchId: 'bucka', text: 'To je bučka.<br>To so bučke.'},
    cebula: {animateTo: 'cebula', matchId: 'cebula', text: 'To je čebula.'},
    cesen: {animateTo: 'cesen', matchId: 'cesen', text: 'To je česen.'},
    cvetaca: {animateTo: 'cvetaca', matchId: 'cvetaca', text: 'To je cvetača.'},
    fizol: {animateTo: 'fizol', matchId: 'fizol', text: 'To je fižol.'},
    grah: {animateTo: 'grah', matchId: 'grah', text: 'To je grah.'},
    korencek: {animateTo: 'korencek', matchId: 'korencek', text: 'To je korenček.'},
    koruza: {animateTo: 'koruza', matchId: 'koruza', text: 'To je koruza.'},
    krompir: {animateTo: 'krompir', matchId: 'krompir', text: 'To je krompir.'},
    kumara: {animateTo: 'kumara', matchId: 'kumara', text: 'To je kumara.<br>To so kumare.'},
    paprika: {animateTo: 'paprika', matchId: 'paprika', text: 'To je paprika.<br>To so paprike.'},
    paradiznik: {animateTo: 'paradiznik', matchId: 'paradiznik', text: 'To je paradižnik.'},
    por: {animateTo: 'por', matchId: 'por', text: 'To je por.'},
    spinaca: {animateTo: 'spinaca', matchId: 'spinaca', text: 'To je špinača.'}
  }]
])
