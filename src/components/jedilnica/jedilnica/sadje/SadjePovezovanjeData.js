export let targetsData = new Map([
  ['SadjePovezovanje', {
    banana: {matchId: 'banana', pos: {x: 410, y: 16}},
    hruska: {matchId: 'hruska', pos: {x: 404, y: 238}},
    limona: {matchId: 'limona', pos: {x: 176, y: 36}},
    sliva: {matchId: 'sliva', pos: {x: 800, y: 161}},
    jagoda: {matchId: 'jagoda', pos: {x: 816, y: 260}},
    pomaranca: {matchId: 'pomaranca', pos: {x: 1140, y: 296}},
    marelica: {matchId: 'marelica', pos: {x: 1156, y: 124}},
    malina: {matchId: 'malina', pos: {x: 698, y: 53}},
    borovnica: {matchId: 'borovnica', pos: {x: 648, y: 160}},
    cesnja: {matchId: 'cesnja', pos: {x: 871, y: 40}},
    lubenica: {matchId: 'lubenica', pos: {x: 910, y: 316}},
    ananas: {matchId: 'ananas', pos: {x: 990, y: 28}},
    kivi: {matchId: 'kivi', pos: {x: 138, y: 176}},
    breskev: {matchId: 'breskev', pos: {x: 198, y: 270}},
    jabolko: {matchId: 'jabolko', pos: {x: 628, y: 294}}
  }]
])

export let labelsData = new Map([
  ['SadjePovezovanje', {
    banana: {animateTo: 'banana', matchId: 'banana', text: 'To je banana.<br>To so banane.'},
    hruska: {animateTo: 'hruska', matchId: 'hruska', text: 'To je hruška.<br>To so hruške.'},
    jagoda: {animateTo: 'jagoda', matchId: 'jagoda', text: 'To je jagoda.<br>To so jagode.'},
    limona: {animateTo: 'limona', matchId: 'limona', text: 'To je limona.<br>To so limone.'},
    sliva: {animateTo: 'sliva', matchId: 'sliva', text: 'To je sliva.<br>To so slive.'},
    pomaranca: {animateTo: 'pomaranca', matchId: 'pomaranca', text: 'To je pomaranča.<br>To so pomaranče.'},
    marelica: {animateTo: 'marelica', matchId: 'marelica', text: 'To je marelica.<br>To so marelice.'},
    malina: {animateTo: 'malina', matchId: 'malina', text: 'To je malina.<br>To so maline.'},
    borovnica: {animateTo: 'borovnica', matchId: 'borovnica', text: 'To je borovnica.<br>To so borovnice.'},
    cesnja: {animateTo: 'cesnja', matchId: 'cesnja', text: 'To je češnja.<br>To so češnje.'},
    breskev: {animateTo: 'breskev', matchId: 'breskev', text: 'To je breskev.<br>To so breskve.'},
    ananas: {animateTo: 'ananas', matchId: 'ananas', text: 'To je ananas.'},
    lubenica: {animateTo: 'lubenica', matchId: 'lubenica', text: 'To je lubenica.'},
    kivi: {animateTo: 'kivi', matchId: 'kivi', text: 'To je kivi.'},
    jabolko: {animateTo: 'jabolko', matchId: 'jabolko', text: 'To je jabolko.<br>To so jabolka.'}
  }]
])
