import banana from '@/assets/jedilnica/jedilnica/sadje/banana.svg'
import hruska from '@/assets/jedilnica/jedilnica/sadje/hruska.svg'
import jagoda from '@/assets/jedilnica/jedilnica/sadje/jagoda.svg'
import limona from '@/assets/jedilnica/jedilnica/sadje/limona.svg'
import sliva from '@/assets/jedilnica/jedilnica/sadje/sliva.svg'
import pomaranca from '@/assets/jedilnica/jedilnica/sadje/pomaranca.svg'
import marelica from '@/assets/jedilnica/jedilnica/sadje/marelica.svg'
import malina from '@/assets/jedilnica/jedilnica/sadje/malina.svg'
import borovnica from '@/assets/jedilnica/jedilnica/sadje/borovnica.svg'
import cesnja from '@/assets/jedilnica/jedilnica/sadje/cesnja.svg'
import breskev from '@/assets/jedilnica/jedilnica/sadje/breskev.svg'
import ananas from '@/assets/jedilnica/jedilnica/sadje/ananas.svg'
import lubenica from '@/assets/jedilnica/jedilnica/sadje/lubenica.svg'
import kivi from '@/assets/jedilnica/jedilnica/sadje/kivi.svg'
import jabolko from '@/assets/jedilnica/jedilnica/sadje/jabolko.svg'

export default {
  banana,
  hruska,
  limona,
  pomaranca,
  marelica,
  malina,
  borovnica,
  cesnja,
  sliva,
  jagoda,
  ananas,
  lubenica,
  kivi,
  breskev,
  jabolko
}
