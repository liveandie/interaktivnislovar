import milo from '@/assets/ucilnice/besede/kajJeTo/items/milo.svg'
import papirnataBrisaca from '@/assets/ucilnice/besede/kajJeTo/items/papirnataBrisaca.svg'
import kosi from '@/assets/ucilnice/besede/kajJeTo/items/kosi.svg'
import umivalnik from '@/assets/ucilnice/besede/kajJeTo/items/umivalnik.svg'
import plakat from '@/assets/ucilnice/besede/kajJeTo/items/plakat.svg'
import loncnica from '@/assets/ucilnice/besede/kajJeTo/items/loncnica.svg'
import miza from '@/assets/ucilnice/besede/kajJeTo/items/miza.svg'
import racunalnik from '@/assets/ucilnice/besede/kajJeTo/items/racunalnik.svg'
import stol from '@/assets/ucilnice/besede/kajJeTo/items/stol.svg'
import ura from '@/assets/ucilnice/besede/kajJeTo/items/ura.svg'
import tabla from '@/assets/ucilnice/besede/kajJeTo/items/tabla.svg'
import vrata from '@/assets/ucilnice/besede/kajJeTo/items/vrata.svg'
import okno from '@/assets/ucilnice/besede/kajJeTo/items/okno.svg'
import omara from '@/assets/ucilnice/besede/kajJeTo/items/omara.svg'
import projektor from '@/assets/ucilnice/besede/kajJeTo/items/projektor.svg'
import klop4 from '@/assets/ucilnice/besede/kajJeTo/items/klop4.svg'
import klop5 from '@/assets/ucilnice/besede/kajJeTo/items/klop5.svg'
import klop6 from '@/assets/ucilnice/besede/kajJeTo/items/klop6.svg'
import klop3 from '@/assets/ucilnice/besede/kajJeTo/items/klop3.svg'
import klop2 from '@/assets/ucilnice/besede/kajJeTo/items/klop2.svg'
import klop1 from '@/assets/ucilnice/besede/kajJeTo/items/klop1.svg'

export default {
  milo,
  papirnataBrisaca,
  kosi,
  umivalnik,
  plakat,
  loncnica,
  ura,
  tabla,
  vrata,
  okno,
  omara,
  projektor,
  klop4,
  klop5,
  klop6,
  klop3,
  klop2,
  klop1,
  miza,
  racunalnik,
  stol
}
