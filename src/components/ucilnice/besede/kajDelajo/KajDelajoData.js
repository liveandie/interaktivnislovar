export let targetsData = new Map([
  ['BesedeKajDelajo', {
    anzePoslusa: {matchId: 'anzePoslusa', pos: {x: 374, y: 362}, size: {w: 70, h: 100}},
    vidSeUci: {matchId: 'vidSeUci', pos: {x: 686, y: 364}, size: {w: 70, h: 100}},
    anjaSedi: {matchId: 'anjaSedi', pos: {x: 682, y: 244}, size: {w: 70, h: 100}},
    peterKlepeta: {matchId: 'peterKlepeta', pos: {x: 964, y: 250}, size: {w: 70, h: 100}},
    // uciteljPise: {matchId: 'uciteljPise', pos: {x: 1118, y: 519}, size: {w: 70, h: 160}},
    katarinaRacuna: {matchId: 'katarinaRacuna', pos: {x: 580, y: 360}, size: {w: 70, h: 100}},
    tinaSeIgra: {matchId: 'tinaSeIgra', pos: {x: 302, y: 252}, size: {w: 70, h: 100}},
    alesRise: {matchId: 'alesRise', pos: {x: 868, y: 254}, size: {w: 70, h: 100}},
    nezaSiUmivaRoke: {matchId: 'nezaSiUmivaRoke', pos: {x: 520, y: 156}, size: {w: 70, h: 200}},
    jasnaBere: {matchId: 'jasnaBere', pos: {x: 268, y: 370}, size: {w: 70, h: 100}},
    jureBriseTablo: {matchId: 'jureBriseTablo', pos: {x: 14, y: 444}, size: {w: 70, h: 200}},
    maticPoje: {matchId: 'maticPoje', pos: {x: 390, y: 244}, size: {w: 70, h: 100}},
    mojcaSlika: {matchId: 'mojcaSlika', pos: {x: 1008, y: 366}, size: {w: 70, h: 100}},
    uciteljicaGovori: {matchId: 'uciteljicaGovori', pos: {x: 138, y: 298}, size: {w: 70, h: 280}}
  }]
])

export let labelsData = new Map([
  ['BesedeKajDelajo', {
    anzePoslusa: {animateTo: 'anzePoslusa', matchId: 'anzePoslusa', text: 'Anže posluša.'},
    vidSeUci: {animateTo: 'vidSeUci', matchId: 'vidSeUci', text: 'Vid se uči.'},
    anjaSedi: {animateTo: 'anjaSedi', matchId: 'anjaSedi', text: 'Anja sedi.'},
    jasnaBere: {animateTo: 'jasnaBere', matchId: 'jasnaBere', text: 'Jasna bere.'},
    peterKlepeta: {animateTo: 'peterKlepeta', matchId: 'peterKlepeta', text: 'Peter klepeta.'},
    // uciteljPise: {animateTo: 'uciteljPise', matchId: 'uciteljPise', text: 'Učitelj piše.'},
    katarinaRacuna: {animateTo: 'katarinaRacuna', matchId: 'katarinaRacuna', text: 'Katarina računa.'},
    tinaSeIgra: {animateTo: 'tinaSeIgra', matchId: 'tinaSeIgra', text: 'Tina se igra.'},
    alesRise: {animateTo: 'alesRise', matchId: 'alesRise', text: 'Aleš riše.'},
    nezaSiUmivaRoke: {animateTo: 'nezaSiUmivaRoke', matchId: 'nezaSiUmivaRoke', text: 'Neža si umiva roke.'},
    jureBriseTablo: {animateTo: 'jureBriseTablo', matchId: 'jureBriseTablo', text: 'Jure briše tablo.'},
    uciteljicaGovori: {animateTo: 'uciteljicaGovori', matchId: 'uciteljicaGovori', text: 'Učiteljica govori.'},
    maticPoje: {animateTo: 'maticPoje', matchId: 'maticPoje', text: 'Matic poje.'},
    mojcaSlika: {animateTo: 'mojcaSlika', matchId: 'mojcaSlika', text: 'Mojca slika.'}
  }]
])

export let inputsData = new Map([
  ['BesedeKajDelajoVpisovanje', {
    anzePoslusa: {openDirection: 'right', arrowDirection: 'left', preText: 'Anže', solution: 'posluša', pos: {x: 454, y: 420}},
    vidSeUci: {openDirection: 'right', arrowDirection: 'left', preText: 'Vid', solution: 'se uči', pos: {x: 778, y: 410}},
    anjaSedi: {openDirection: 'right', arrowDirection: 'left', preText: 'Anja', solution: 'sedi', pos: {x: 754, y: 306}},
    peterKlepeta: {openDirection: 'right', arrowDirection: 'bottom', preText: 'Peter', solution: 'klepeta', pos: {x: 964, y: 174}},
    // uciteljPise: {openDirection: 'left', arrowDirection: 'right', preText: 'Učitelj', solution: 'piše', pos: {x: 1028, y: 636}},
    katarinaRacuna: {openDirection: 'left', arrowDirection: 'right', preText: 'Katarina', solution: 'računa', pos: {x: 510, y: 362}},
    tinaSeIgra: {openDirection: 'left', arrowDirection: 'bottom', preText: 'Tina', solution: 'se igra', pos: {x: 309, y: 187}},
    alesRise: {openDirection: 'left', arrowDirection: 'bottom', preText: 'Aleš', solution: 'riše', pos: {x: 868, y: 172}},
    nezaSiUmivaRoke: {openDirection: 'right', arrowDirection: 'bottom', preText: 'Neža', solution: 'si umiva roke', pos: {x: 524, y: 88}},
    jasnaBere: {openDirection: 'right', arrowDirection: 'right', preText: 'Jasna', solution: 'bere', pos: {x: 202, y: 420}},
    jureBriseTablo: {openDirection: 'right', arrowDirection: 'left', preText: 'Jure', solution: 'briše tablo', pos: {x: 88, y: 550}},
    maticPoje: {openDirection: 'right', arrowDirection: 'left', preText: 'Matic', solution: 'poje', pos: {x: 472, y: 290}},
    mojcaSlika: {openDirection: 'left', arrowDirection: 'left', preText: 'Mojca', solution: 'slika', pos: {x: 1088, y: 360}},
    uciteljicaGovori: {openDirection: 'right', arrowDirection: 'right', preText: 'Učiteljica', solution: 'govori', pos: {x: 64, y: 300}}
  }]])

export let linesData = new Map([
  ['BesedeKajDelajo', {
    kaj: 'Kaj delajo?',
    poskusi: 'Zdaj pa poskusi še ti!'
  }],
  ['BesedeKajDelajoVpisovanje', {
    poskusi: 'Zdaj pa poskusi še ti!'
  }]
])
