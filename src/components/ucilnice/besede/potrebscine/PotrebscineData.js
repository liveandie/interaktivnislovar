export let targetsData = new Map([
  ['Potrebscine-2', {
    torba: {matchId: 'torba', pos: {x: 81, y: 38}},
    list: {matchId: 'list', pos: {x: 177, y: 378}},
    delovniZvezek: {matchId: 'delovniZvezek', pos: {x: 515, y: 292}},
    ucbenik: {matchId: 'ucbenik', pos: {x: 524, y: 180}},
    mapa: {matchId: 'mapa', pos: {x: 686, y: 158}},
    urnik: {matchId: 'urnik', pos: {x: 721, y: 351}},
    ura: {matchId: 'ura', pos: {x: 936, y: 401}},
    knjiga: {matchId: 'knjiga', pos: {x: 896, y: 299}},
    berilo: {matchId: 'berilo', pos: {x: 943, y: 40}},
    vodenke: {matchId: 'vodenke', pos: {x: 1078, y: 203}},
    zvezek: {matchId: 'zvezek', pos: {x: 1074, y: 327}}
  }],
  ['Potrebscine-3', {
    barvice: {matchId: 'barvice', pos: {x: 37, y: 207}},
    flomaster: {matchId: 'flomaster', pos: {x: 790, y: 165}},
    geotrikotnik: {matchId: 'geotrikotnik', pos: {x: 571, y: 179}},
    goba: {matchId: 'goba', pos: {x: 988, y: 99}},
    kemicniSvincnik: {matchId: 'kemicniSvincnik', pos: {x: 630, y: 309}},
    kreda: {matchId: 'kreda', pos: {x: 1036, y: 322}},
    lepilo: {matchId: 'lepilo', pos: {x: 1068, y: 352}},
    nalivnik: {matchId: 'nalivnik', pos: {x: 432, y: 321}},
    peresnica: {matchId: 'peresnica', pos: {x: 208, y: 93}},
    radirka: {matchId: 'radirka', pos: {x: 727, y: 398}},
    ravnilo: {matchId: 'ravnilo', pos: {x: 43, y: 320}},
    sestilo: {matchId: 'sestilo', pos: {x: 278, y: 315}},
    silcek: {matchId: 'silcek', pos: {x: 870, y: 245}},
    skarje: {matchId: 'skarje', pos: {x: 384, y: 199}},
    sponka: {matchId: 'sponka', pos: {x: 574, y: 374}},
    svincnik: {matchId: 'svincnik', pos: {x: 824, y: 354}}
  }]
])

export let labelsData = new Map([
  ['Potrebscine-2', {
    torba: {animateTo: 'torba', matchId: 'torba', text: 'To je torba.', pos: {x: 79, y: 607}},
    list: {animateTo: 'list', matchId: 'list', text: 'To je list.', pos: {x: 992, y: 535}},
    delovniZvezek: {animateTo: 'delovniZvezek', matchId: 'delovniZvezek', text: 'To je delovni zvezek.', pos: {x: 158, y: 674}},
    ucbenik: {animateTo: 'ucbenik', matchId: 'ucbenik', text: 'To je učbenik.', pos: {x: 369, y: 607}},
    mapa: {animateTo: 'mapa', matchId: 'mapa', text: 'To je mapa.', pos: {x: 917, y: 674}},
    urnik: {animateTo: 'urnik', matchId: 'urnik', text: 'To je urnik.', pos: {x: 695, y: 607}},
    ura: {animateTo: 'ura', matchId: 'ura', text: 'To je ura.', pos: {x: 135, y: 535}},
    knjiga: {animateTo: 'knjiga', matchId: 'knjiga', text: 'To je knjiga.', pos: {x: 690, y: 535}},
    berilo: {animateTo: 'berilo', matchId: 'berilo', text: 'To je berilo.', pos: {x: 393, y: 535}},
    vodenke: {animateTo: 'vodenke', matchId: 'vodenke', text: 'To so vodenke.', pos: {x: 981, y: 607}},
    zvezek: {animateTo: 'zvezek', matchId: 'zvezek', text: 'To je zvezek.', pos: {x: 609, y: 674}}
  }],
  ['Potrebscine-3', {
    barvice: {animateTo: 'barvice', matchId: 'barvice', text: 'To so barvice.', pos: {x: 1006, y: 607}},
    flomaster: {animateTo: 'flomaster', matchId: 'flomaster', text: 'To je flomaster.', pos: {x: 657, y: 607}},
    geotrikotnik: {animateTo: 'geotrikotnik', matchId: 'geotrikotnik', text: 'To je geotrikotnik.', pos: {x: 64, y: 462}},
    goba: {animateTo: 'goba', matchId: 'goba', text: 'To je goba.', pos: {x: 39, y: 674}},
    kemicniSvincnik: {animateTo: 'kemicniSvincnik', matchId: 'kemicniSvincnik', text: 'To je kemični svinčnik.', pos: {x: 281, y: 674}},
    kreda: {animateTo: 'kreda', matchId: 'kreda', text: 'To je kreda.', pos: {x: 1008, y: 535}},
    lepilo: {animateTo: 'lepilo', matchId: 'lepilo', text: 'To je lepilo.', pos: {x: 1040, y: 462}},
    nalivnik: {animateTo: 'nalivnik', matchId: 'nalivnik', text: 'To je nalivnik.', pos: {x: 369, y: 535}},
    peresnica: {animateTo: 'peresnica', matchId: 'peresnica', text: 'To je peresnica.', pos: {x: 674, y: 535}},
    radirka: {animateTo: 'radirka', matchId: 'radirka', text: 'To je radirka.', pos: {x: 63, y: 607}},
    ravnilo: {animateTo: 'ravnilo', matchId: 'ravnilo', text: 'To je ravnilo.', pos: {x: 1016, y: 674}},
    sestilo: {animateTo: 'sestilo', matchId: 'sestilo', text: 'To je šestilo.', pos: {x: 739, y: 674}},
    silcek: {animateTo: 'silcek', matchId: 'silcek', text: 'To je šilček.', pos: {x: 108, y: 535}},
    skarje: {animateTo: 'skarje', matchId: 'skarje', text: 'To so škarje.', pos: {x: 460, y: 462}},
    sponka: {animateTo: 'sponka', matchId: 'sponka', text: 'To je sponka.', pos: {x: 750, y: 462}},
    svincnik: {animateTo: 'svincnik', matchId: 'svincnik', text: 'To je svinčnik.', pos: {x: 354, y: 607}}
  }]
])

export let linesData = new Map([
  ['Potrebscine-2', {
    nowYou: 'Zdaj pa poskusi še ti.'
  }],
  ['Potrebscine-3', {
    nowYou: 'Zdaj pa poskusi še ti.'
  }]
])
