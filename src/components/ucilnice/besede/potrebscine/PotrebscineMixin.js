import { TweenLite } from 'gsap'
import { Observable, Subject } from 'rxjs'

import GameBasicMixin from '@/components/shared/mixins/GameBasicMixin'
import GameUtilsMixin from '@/components/shared/mixins/GameUtilsMixin'
import StoreLabelsMixin from '@/components/shared/mixins/StoreLabelsMixin'
import StoreTargetsMixin from '@/components/shared/mixins/StoreTargetsMixin'
import AnimationsLabelsMixin from '@/components/shared/mixins/AnimationsLabelsMixin'

import characterAnim from '@/assets/ucilnice/besede/potrebscine/animations/potrebscine-jakob.json'
import { targetsData, labelsData, linesData } from './PotrebscineData'

export default {
  mixins: [
    GameBasicMixin,
    StoreLabelsMixin,
    StoreTargetsMixin,
    GameUtilsMixin,
    AnimationsLabelsMixin
  ],
  data () {
    return {
      targetsData: {},
      labelsData: {},
      characterAnim: characterAnim
    }
  },

  methods: {
    onDragStopped (dragEl, dragElPos) {
      let target = this.getTargetIfMatch(this.targets, dragEl.matchId, dragElPos)

      if (target) {
        target.setSolved()
        dragEl.setSolved()
        if (this.isGameSolved()) {
          this.startEnd()
        }
      } else {
        dragEl.moveToStartPosition()
      }
    },

    isGameSolved () {
      let solved = this.labels.filter((label) => label.isSolved)
      return solved.length === this.labels.length
    },

    resetScene () {
      this.$refs.endModal.hide()
      this.targets.forEach((target) => target.reset())
      this.labels.forEach((label) => label.reset())
      this.bubble.hide()
    },

    startIntroAnimation () {
      this.resetScene()
      this.setStateToIntro()

      this.introAnim$ = this.animateLabelsToTargets$(this.labels, this.$refs)
        .flatMap(() => this.slideJakob())
        .flatMap(() => Observable.zip(
          this.bubble.show$(this.linesData.nowYou),
          this.jakob.play$(false, true))
          .do(() => this.bubble.hide())
        )
        .flatMap(() => this.slideJakob(true))
        .subscribe(null, null, () => this.startInteraction())
    },

    stopIntroAnimation () {
      if (this.introAnim$) {
        this.introAnim$.unsubscribe()
      }

      this.jakob.currentX = -522
      this.bubble.hide()
    },

    startInteraction () {
      this.resetScene()
      this.setStateToInteractive()
    },

    startEnd () {
      this.setStateToEnd()

      const dialog = this.endDialogs.get(this.$route.name)
      this.$refs.endModal.show(dialog)
    },

    getLabelDestination (label) {
      let destination = this.$refs[label.animateTo][0].center()
      let labelBounds = label.bounds()

      return {
        currentX: destination.x - labelBounds.width / 2,
        currentY: destination.y - labelBounds.height / 2
      }
    },

    slideJakob (out = false) {
      let completed$ = new Subject()
      this.jakobSlide = TweenLite.to(this.jakob, 1, {
        currentX: out ? -522 : 0,
        onComplete: () => completed$.next()
      })

      return Observable.of('').do(() => {
        this.jakobSlide.play$()
      }).flatMap((_) => completed$.take(1))
    }
  },

  created () {
    this.targetsData = targetsData.get(this.$route.name)
    this.labelsData = labelsData.get(this.$route.name)
    this.linesData = linesData.get(this.$route.name)

    this.$store.dispatch('setShowNavBack', true)
    this.$store.dispatch('setShowNavForward', true)
  },

  mounted () {
    this.labels.forEach((label) => {
      let b = label.$el.getBoundingClientRect()
      label.setPosition(
        (b.left - this.screenPosition.x) / this.screenScale,
        (b.top - this.screenPosition.y) / this.screenScale
      )
    })

    this.$refs.positioning.classList.remove('positioning')

    this.jakob = this.$refs.jakob
    this.bubble = this.$refs.bubble

    this.startIntroAnimation()
  },

  destroyed () {
    this.stopIntroAnimation()

    if (this.introTimeline) {
      this.introTimeline.kill()
    }

    if (this.intro) {
      this.intro.kill()
    }

    if (this.jakobSlide) {
      this.jakobSlide.kill()
    }
  }
}
