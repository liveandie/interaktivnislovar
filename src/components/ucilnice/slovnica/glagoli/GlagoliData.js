export let linesData = new Map([
  ['Glagoli-1', {
    jaz: 'Jaz piše<span style="color: red;">m</span>.',
    ti: 'Ti piše<span style="color: red;">š</span>.',
    on: 'On piše.'
  }],
  ['Glagoli-2', {
    midva: 'Midva piše<span style="color: red;">va</span>.',
    vidva: 'Vidva piše<span style="color: red;">ta</span>.',
    onadva: 'Onadva piše<span style="color: red;">ta</span>.'
  }],
  ['Glagoli-3', {
    mi: 'Mi piše<span style="color: red;">mo</span>.',
    vi: 'Vi piše<span style="color: red;">te</span>.',
    oni: 'Oni piše<span style="color: red;">jo</span>.'
  }]
])
