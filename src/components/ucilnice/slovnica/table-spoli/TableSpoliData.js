export let labelsData = new Map([
  ['TableSpoli-1', {
    urnik: {matchId: 'b1', text: 'urnik'},
    svincnik: {matchId: 'b1', text: 'svinčnik'},
    geotrikotnik: {matchId: 'b1', text: 'geotrikotnik'},
    ucitelj: {matchId: 'b1', text: 'učitelj'},
    zvocnik: {matchId: 'b1', text: 'zvočnik'},
    kos: {matchId: 'b1', text: 'koš'},
    silcek: {matchId: 'b1', text: 'šilček'},
    projektor: {matchId: 'b1', text: 'projektor'},
    sosolec: {matchId: 'b1', text: 'sošolec'},
    plakat: {matchId: 'b1', text: 'plakat'},
    omara: {matchId: 'b2', text: 'omara'},
    kreda: {matchId: 'b2', text: 'kreda'},
    goba: {matchId: 'b2', text: 'goba'},
    peresnica: {matchId: 'b2', text: 'peresnica'},
    torba: {matchId: 'b2', text: 'torba'},
    knjiga: {matchId: 'b2', text: 'knjiga'},
    miza: {matchId: 'b2', text: 'miza'},
    tabla: {matchId: 'b2', text: 'tabla'},
    sestilo: {matchId: 'b3', text: 'šestilo'},
    milo: {matchId: 'b3', text: 'milo'},
    berilo: {matchId: 'b3', text: 'berilo'}
  }],
  ['TableSpoli-2', {
    racunalnik: {matchId: 'b1', text: 'računalnik'},
    stol: {matchId: 'b1', text: 'stol'},
    delovniZvezek: {matchId: 'b1', text: 'delovni zvezek'},
    ucbenik: {matchId: 'b1', text: 'učbenik'},
    list: {matchId: 'b1', text: 'list'},
    ucenec: {matchId: 'b1', text: 'učenec'},
    umivalnik: {matchId: 'b1', text: 'umivalnik'},
    zvezek: {matchId: 'b1', text: 'zvezek'},
    flomaster: {matchId: 'b1', text: 'flomaster'},
    uciteljica: {matchId: 'b2', text: 'učiteljica'},
    mapa: {matchId: 'b2', text: 'mapa'},
    radirka: {matchId: 'b2', text: 'radirka'},
    sponka: {matchId: 'b2', text: 'sponka'},
    ura: {matchId: 'b2', text: 'ura'},
    loncnica: {matchId: 'b2', text: 'lončnica'},
    ucenka: {matchId: 'b2', text: 'učenka'},
    sosolka: {matchId: 'b2', text: 'sošolka'},
    okno: {matchId: 'b3', text: 'okno'},
    ravnilo: {matchId: 'b3', text: 'ravnilo'},
    lepilo: {matchId: 'b3', text: 'lepilo'}
  }]
])
