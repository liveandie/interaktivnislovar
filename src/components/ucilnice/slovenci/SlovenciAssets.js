import francePreseren from '@/assets/ucilnice/slovenci/francePreseren.svg'
import ivanaKobilca from '@/assets/ucilnice/slovenci/ivanaKobilca.svg'
import ivanCankar from '@/assets/ucilnice/slovenci/ivanCankar.svg'
import jakobPetelin from '@/assets/ucilnice/slovenci/jakobPetelin.svg'
import janezValvazor from '@/assets/ucilnice/slovenci/janezValvazor.svg'
import jozePlecnik from '@/assets/ucilnice/slovenci/jozePlecnik.svg'
import jurijVega from '@/assets/ucilnice/slovenci/jurijVega.svg'
import primozTrubar from '@/assets/ucilnice/slovenci/primozTrubar.svg'
import rihardJakopic from '@/assets/ucilnice/slovenci/rihardJakopic.svg'

export default {
  francePreseren,
  ivanaKobilca,
  ivanCankar,
  jakobPetelin,
  janezValvazor,
  jozePlecnik,
  jurijVega,
  primozTrubar,
  rihardJakopic
}
