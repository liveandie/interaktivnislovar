export let targetsData = new Map([
  ['KdoJeTo', {
    ivanaKobilca: {matchId: 'ivanaKobilca', pos: {x: 125, y: 60}},
    primozTrubar: {matchId: 'primozTrubar', pos: {x: 310, y: 60}},
    francePreseren: {matchId: 'francePreseren', pos: {x: 495, y: 60}},
    jozePlecnik: {matchId: 'jozePlecnik', pos: {x: 690, y: 60}},
    jurijVega: {matchId: 'jurijVega', pos: {x: 865, y: 60}},
    ivanCankar: {matchId: 'ivanCankar', pos: {x: 210, y: 290}},
    jakobPetelin: {matchId: 'jakobPetelin', pos: {x: 395, y: 290}},
    janezValvazor: {matchId: 'janezValvazor', pos: {x: 580, y: 290}},
    rihardJakopic: {matchId: 'rihardJakopic', pos: {x: 765, y: 290}}
  }]
])

export let labelsData = new Map([
  ['KdoJeTo', {
    ivanaKobilca: {animateTo: 'ivanaKobilca', matchId: 'ivanaKobilca', text: 'To je Ivana Kobilca, ki je slikarka.'},
    primozTrubar: {animateTo: 'primozTrubar', matchId: 'primozTrubar', text: 'To je Primož Trubar, ki je pisatelj.'},
    francePreseren: {animateTo: 'francePreseren', matchId: 'francePreseren', text: 'To je France Prešeren, ki je pesnik.'},
    jozePlecnik: {animateTo: 'jozePlecnik', matchId: 'jozePlecnik', text: 'To je Jože Plečnik, ki je arhitekt.'},
    jurijVega: {animateTo: 'jurijVega', matchId: 'jurijVega', text: 'To je Jurij Vega, ki je matematik.'},
    ivanCankar: {animateTo: 'ivanCankar', matchId: 'ivanCankar', text: 'To je Ivan Cankar, ki je pisatelj.'},
    jakobPetelin: {animateTo: 'jakobPetelin', matchId: 'jakobPetelin', text: 'To je Jakob Petelin, ki je skladatelj.'},
    janezValvazor: {animateTo: 'janezValvazor', matchId: 'janezValvazor', text: 'To je Janez Vajkard Valvazor, ki je zgodovinar.'},
    rihardJakopic: {animateTo: 'rihardJakopic', matchId: 'rihardJakopic', text: 'To je Rihard Jakopič, ki je slikar.'}
  }]
])

export let linesData = new Map([
  ['KdoJeTo', {
    poskusi: 'Zdaj pa poskusi še ti!'
  }]
])
