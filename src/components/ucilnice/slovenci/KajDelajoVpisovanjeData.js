export let itemsData = new Map([
  ['KajDelajoVpisovanje', {
    ivanaKobilca: {pos: {x: 125, y: 60}},
    primozTrubar: {pos: {x: 310, y: 60}},
    francePreseren: {pos: {x: 495, y: 60}},
    jozePlecnik: {pos: {x: 690, y: 60}},
    jurijVega: {pos: {x: 865, y: 60}},
    ivanCankar: {pos: {x: 210, y: 290}},
    jakobPetelin: {pos: {x: 395, y: 290}},
    janezValvazor: {pos: {x: 580, y: 290}},
    rihardJakopic: {pos: {x: 765, y: 290}}
  }]
])

export let inputsData = new Map([
  ['KajDelajoVpisovanje', {
    rihardJakopic: {openDirection: 'right', arrowDirection: 'top', preText: 'Rihard Jakopič', solution: 'slika', pos: {x: 800, y: 460}},
    janezValvazor: {openDirection: 'right', arrowDirection: 'top', preText: 'Janez Vajkard Valvazor', solution: 'piše', pos: {x: 615, y: 460}},
    jakobPetelin: {openDirection: 'right', arrowDirection: 'top', preText: 'Jakob Petelin', solution: 'sklada', pos: {x: 430, y: 460}},
    ivanCankar: {openDirection: 'right', arrowDirection: 'top', preText: 'Ivan Cankar', solution: 'piše', pos: {x: 245, y: 460}},
    jurijVega: {openDirection: 'right', arrowDirection: 'top', preText: 'Jurij Vega', solution: 'računa', pos: {x: 900, y: 230}},
    jozePlecnik: {openDirection: 'right', arrowDirection: 'top', preText: 'Jože Plečnik', solution: 'riše', pos: {x: 715, y: 230}},
    francePreseren: {openDirection: 'right', arrowDirection: 'top', preText: 'France Prešeren', solution: 'piše', pos: {x: 530, y: 230}},
    primozTrubar: {openDirection: 'right', arrowDirection: 'top', preText: 'Primož Trubar', solution: 'piše', pos: {x: 345, y: 230}},
    ivanaKobilca: {openDirection: 'right', arrowDirection: 'top', preText: 'Ivana Kobilca', solution: 'slika', pos: {x: 160, y: 230}}
  }]
])

export let linesData = new Map([
  ['KajDelajoVpisovanje', {
    kaj: 'Kaj delajo?',
    poskusi: 'Tdaj pa poskusi še ti!'
  }]
])
