export default {
  components: {},

  data () {
    return {
      originalX: 0,
      originalY: 0,
      currentX: 0,
      currentY: 0
    }
  },

  props: {
    posX: Number,
    posY: Number
  },

  computed: {
    transformStyle () {
      return {transform: `translate(${this.currentX}px, ${this.currentY}px)`}
    }
  },

  methods: {
    setPosition (x, y) {
      this.originalX = this.currentX = x
      this.originalY = this.currentY = y
    }
  },

  mounted () {
    this.originalX = this.currentX = this.posX
    this.originalY = this.currentY = this.posY
  }
}
