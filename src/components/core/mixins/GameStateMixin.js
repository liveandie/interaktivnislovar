export default {
  data () {
    return {
      gameStates: {
        intro: 'intro',
        interactive: 'interactive',
        end: 'end'
      },
      gameState: 'intro'
    }
  },

  methods: {
    setStateToIntro () {
      this.gameState = this.gameStates.intro
    },

    setStateToInteractive () {
      this.gameState = this.gameStates.interactive
    },

    setStateToEnd () {
      this.gameState = this.gameStates.end
    }
  }
}
