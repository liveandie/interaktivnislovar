import { Observable } from 'rxjs'
import { TimelineLite } from 'gsap'

export default {
  methods: {
    animateLabelsToTargets$ (labels, refs) {
      return Observable.create((observer) => {
        let introTimeline = new TimelineLite({
          onComplete: () => {
            observer.next()
            observer.complete()
          },
          delay: 0.6,
          paused: true})

        labels.forEach((label) => {
          let destination = refs[label.animateTo][0].center()
          let labelBounds = label.$el.getBoundingClientRect()

          introTimeline
            .set(label.$el, {'z-index': 100})
            .to(label, 2, {
              currentX: destination.x - (labelBounds.width / this.screenScale) / 2,
              currentY: destination.y - (labelBounds.height / this.screenScale) / 2
            })
            .set(label, {isSolved: true})
        })

        introTimeline.play()

        return () => {
          introTimeline.kill()
        }
      })
    }
  }
}
