import SvgSprite from '@/components/core/SvgSprite.vue'
import LabelComponent from '@/components/shared/LabelComponent.vue'
import InteractiveTargetComponent from '@/components/shared/InteractiveTargetComponent.vue'
import SvgSpriteComponent from '@/components/shared/SvgSpriteComponent.vue'
import BMAnim from '@/components/core/BMAnimation.vue'
import Bubble from '@/components/shared/BubbleComponent.vue'
import Game from '@/components/core/Game.vue'

import EndGameModalComponent from '@/components/shared/EndGameModalComponent.vue'
import GameStateMixin from '@/components/core/mixins/GameStateMixin'
import NavigationMixin from '@/components/core/mixins/NavigationMixin'

export default {
  components: {
    'is-game': Game,
    'is-svg-sprite': SvgSprite,
    'is-svg-sprite-pos': SvgSpriteComponent,
    'is-label': LabelComponent,
    'is-interactive-target': InteractiveTargetComponent,
    'is-bm-anim': BMAnim,
    'is-bubble': Bubble,
    'is-end-game-modal': EndGameModalComponent
  },
  mixins: [
    NavigationMixin,
    GameStateMixin
  ],
  data () {
    return {}
  },
  computed: {
    screenPosition () {
      return this.$store.state.screenPosition
    },
    screenScale () {
      return this.$store.state.screenScale
    },
    forwardRoutes () {
      return this.$store.state.sceneRoutes.forwardRoutes
    },
    backwardRoutes () {
      return this.$store.state.sceneRoutes.backwardRoutes
    },
    endDialogs () {
      return this.$store.state.sceneRoutes.endDialogs
    }
  },

  methods: {
    navigateBack () {
      switch (this.gameState) {
        case this.gameStates.intro:
          const route = this.toRouteName(this.backwardRoutes, this.$route.name)
          this.$router.push({name: route})
          break

        case this.gameStates.interactive:
          this.startIntroAnimation()
          break

        case this.gameStates.end:
          this.startInteraction()
          break
      }
    },

    navigateForward () {
      switch (this.gameState) {
        case this.gameStates.intro:
          this.stopIntroAnimation()
          this.startInteraction()
          break

        case this.gameStates.interactive:
        case this.gameStates.end:
          const route = this.toRouteName(this.forwardRoutes, this.$route.name)
          this.$router.push({name: route})
          break
      }
    },

    toRouteName (pool, fromRoute) {
      if (!pool.has(fromRoute)) {
        throw new Error(`No to route defined from path ${fromRoute}`)
      }

      return pool.get(fromRoute)
    },

    startIntroAnimation () {},
    stopIntroAnimation () {},
    startInteraction () {}
  }
}
