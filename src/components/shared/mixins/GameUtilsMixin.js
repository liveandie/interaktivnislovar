export default {
  methods: {
    getTargetIfMatch (targets, matchToId, pos) {
      for (let target of targets) {
        let bounds = target.bounds()
        let isInBounds = this.checkIfInBounds(bounds, pos, this.screenScale, this.screenPosition.x)

        if (isInBounds && target.matchId === matchToId) {
          return target
        }
      }

      return null
    },
    checkIfInBounds (bounds, pos, scale, offset) {
      return pos.x > (bounds.left - offset) / scale &&
        pos.y > bounds.top / scale &&
        pos.x < (bounds.right - offset) / scale &&
        pos.y < bounds.bottom / scale
    },
    shuffleArray (array) {
      let shuffled = Array.from(array)
      for (let i = shuffled.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1))
        let temp = shuffled[i]
        shuffled[i] = shuffled[j]
        shuffled[j] = temp
      }
      return shuffled
    }
  }
}
