export default {
  data () {
    return {
      targets: []
    }
  },
  mounted () {
    this.targets = Object.keys(this.$refs)
      .filter(val => val.includes('$t'))
      .map(key => this.$refs[key][0])
  }
}
