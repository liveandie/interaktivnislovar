export default {
  data () {
    return {
      labels: []
    }
  },
  mounted () {
    this.labels = Object.keys(this.$refs)
      .filter(val => val.includes('$l'))
      .map(key => this.$refs[key][0])
  }
}
