const state = {
  // Determines which end dialog pops up
  // 0 - no dialog; 1 - next game screen dialog; 2 - next game dialog; 3 - end of section dialog
  endDialogs: new Map([
    ['KajJeTo-1', 1],
    ['KajJeTo-2', 2],
    ['Potrebscine-2', 1],
    ['Potrebscine-3', 2],
    ['KajJeToVpisovanje', 3],
    ['BesedeKajDelajo', 2],
    ['BesedeKajDelajoVpisovanje', 3],

    ['TablePovezovanje-2', 2],
    ['TablePovezovanje', 2],
    ['TableVpisovanje', 2],
    ['TableSpoli-1', 1],
    ['TableSpoli-2', 2],

    ['SlovenciUvod', 0],
    ['KdoJeTo', 2],
    ['KajDelajoVpisovanje', 3],

    ['KosiloPovezovanje', 3],
    ['MalicaPovezovanje', 3],
    ['SadjePovezovanje', 3],
    ['ZelenjavaPovezovanje', 3],

    ['JedilniPripomockiPovezovanje', 2],
    ['JedilniPripomockiVpisovanje', 3],

    ['KosareIzbiranje', 2],
    ['SadnaKupaSestavi', 3]
  ]),

  forwardRoutes: new Map([
    ['NaslovnaStran', 'UvodnaAnimacija'],
    ['UvodnaAnimacija', 'Avla'],

    ['Avla', {
      ucilnice: 'Ucilnice',
      jedilnica: 'Jedilnica'
    }],

    ['Ucilnice', {
      besede: 'BesedeUvod',
      slovnica: 'TableSpoli-1',
      slovenci: 'SlovenciUvod'
    }],

    ['BesedeUvod', 'KajJeTo-1'],
    ['KajJeTo-1', 'KajJeTo-2'],
    ['KajJeTo-2', 'Potrebscine-2'],
    ['Potrebscine-2', 'Potrebscine-3'],
    ['Potrebscine-3', 'KajJeToVpisovanje'],
    ['KajJeToVpisovanje', 'BesedeKajDelajo'],
    ['BesedeKajDelajo', 'BesedeKajDelajoVpisovanje'],
    ['BesedeKajDelajoVpisovanje', 'Ucilnice'],

    ['TableSpoli-1', 'TableSpoli-2'],
    ['TableSpoli-2', 'Glagoli-1'],
    ['Glagoli-1', 'Glagoli-2'],
    ['Glagoli-2', 'Glagoli-3'],
    ['Glagoli-3', 'TablePovezovanje'],
    ['TablePovezovanje', 'TablePovezovanje-2'],
    ['TablePovezovanje-2', 'TableVpisovanje'],
    ['TableVpisovanje', 'Ucilnice'],

    ['SlovenciUvod', 'KdoJeTo'],
    ['KdoJeTo', 'KajDelajoVpisovanje'],
    ['KajDelajoVpisovanje', 'Ucilnice'],

    ['Jedilnica', {
      sadje: 'SadjePovezovanje',
      zelenjava: 'ZelenjavaPovezovanje',
      malica: 'MalicaPovezovanje',
      kosilo: 'KosiloPovezovanje',
      kuhinja: 'Kuhinja'
    }],

    ['KosiloPovezovanje', 'Jedilnica'],
    ['MalicaPovezovanje', 'Jedilnica'],
    ['SadjePovezovanje', 'Jedilnica'],
    ['ZelenjavaPovezovanje', 'Jedilnica'],

    ['Kuhinja', 'Pripomocki'],

    ['JedilniPripomockiPovezovanje', 'JedilniPripomockiVpisovanje'],
    ['JedilniPripomockiVpisovanje', 'Kuhinja'],

    ['KosareIzbiranje', 'SadnaKupaSestavi'],
    ['SadnaKupaSestavi', 'Jedilnica']
  ]),

  backwardRoutes: new Map([
    ['Ucilnice', 'Avla'],

    ['BesedeUvod', 'Ucilnice'],
    ['KajJeTo-1', 'BesedeUvod'],
    ['KajJeTo-2', 'KajJeTo-1'],
    ['Potrebscine-2', 'KajJeTo-2'],
    ['Potrebscine-3', 'Potrebscine-2'],
    ['KajJeToVpisovanje', 'Potrebscine-3'],
    ['BesedeKajDelajo', 'KajJeToVpisovanje'],
    ['BesedeKajDelajoVpisovanje', 'BesedeKajDelajo'],

    ['TableSpoli-1', 'Ucilnice'],
    ['TableSpoli-2', 'TableSpoli-1'],
    ['Glagoli-1', 'TableSpoli-2'],
    ['Glagoli-2', 'Glagoli-1'],
    ['Glagoli-3', 'Glagoli-2'],
    ['TablePovezovanje', 'Glagoli-3'],
    ['TablePovezovanje-2', 'TablePovezovanje'],
    ['TableVpisovanje', 'TablePovezovanje-2'],

    ['SlovenciUvod', 'Ucilnice'],
    ['KdoJeTo', 'SlovenciUvod'],
    ['KajDelajoVpisovanje', 'KdoJeTo'],

    ['Jedilnica', 'Avla'],

    ['KosiloPovezovanje', 'Jedilnica'],
    ['MalicaPovezovanje', 'Jedilnica'],
    ['SadjePovezovanje', 'Jedilnica'],
    ['ZelenjavaPovezovanje', 'Jedilnica'],

    ['Kuhinja', 'Jedilnica'],

    ['JedilniPripomockiPovezovanje', 'Kuhinja'],
    ['JedilniPripomockiVpisovanje', 'JedilniPripomockiPovezovanje'],

    ['KosareIzbiranje', 'Jedilnica'],
    ['SadnaKupaSestavi', 'KosareIzbiranje']
  ])
}

const mutations = {}

const actions = {}

const getters = {}

export default {
  state,
  getters,
  actions,
  mutations
}
