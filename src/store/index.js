import Vue from 'vue'
import Vuex from 'vuex'
import SceneRoutes from './modules/sceneRoutes'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  state: {
    screenPosition: {x: 0, y: 0},
    screenScale: 1,
    screenSize: {width: 1334, height: 750},

    navBackText: '',
    navForwardText: '',
    showNavBack: false,
    showNavForward: false,
    pulseNavBack: false,
    pulseNavForward: false
  },
  mutations: {
    updateScreenPosition (state, position) {
      state.screenPosition = position
    },
    updateScreenScale (state, scale) {
      state.screenScale = scale
    },
    updateScreenSize (state, size) {
      state.screenSize = size
    },

    setNavBackText (state, name) {
      state.navBackText = name
    },
    setNavForwardText (state, name) {
      state.navForwardText = name
    },
    setShowNavBack (state, show) {
      state.showNavBack = show
    },
    setShowNavForward (state, show) {
      state.showNavForward = show
    },
    setPulseNavBack (state, pulse) {
      state.pulseNavBack = pulse
    },
    setPulseNavForward (state, pulse) {
      state.pulseNavForward = pulse
    }
  },
  actions: {
    updateScreenPosition ({commit}, position) {
      commit('updateScreenPosition', position)
    },
    updateScreenScale ({commit}, scale) {
      commit('updateScreenScale', scale)
    },
    updateScreenSize ({commit}, size) {
      commit('updateScreenSize', size)
    },

    setNavBackText ({commit}, name) {
      commit('setNavBackText', name)
    },
    setNavForwardText ({commit}, name) {
      commit('setNavForwardText', name)
    },
    setShowNavBack ({commit}, show) {
      commit('setShowNavBack', show)
    },
    setShowNavForward ({commit}, show) {
      commit('setShowNavForward', show)
    },
    setPulseNavForward ({commit}, pulse) {
      commit('setPulseNavForward', pulse)
    },
    setPulseNavBack ({commit}, pulse) {
      commit('setPulseNavBack', pulse)
    }
  },
  modules: {
    sceneRoutes: SceneRoutes
  },
  strict: debug
})
