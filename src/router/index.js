import Vue from 'vue'
import Router from 'vue-router'

import NotFoundContainer from '@/components/NotFoundContainer'

import IntroTitleContainer from '@/components/intro/IntroTitleContainer'
import IntroAnimationContainer from '@/components/intro/IntroAnimationContainer'

import AvlaContainer from '@/components/avla/AvlaContainer'
import UcilniceContainer from '@/components/ucilnice/UcilniceContainer'

import GameContainer from '@/components/core/GameContainer'

import BesedeIntroContainer from '@/components/ucilnice/besede/intro/BesedeIntroContainer'

import KajJeToContainer from '@/components/ucilnice/besede/kajJeTo/KajJeToContainer'
import KajJeToVpisovanjeContainer from '@/components/ucilnice/besede/kajJeTo/KajJeToVpisovanjeContainer'
import PotrebscineS2Container from '@/components/ucilnice/besede/potrebscine/PotrebscineS2Container'
import PotrebscineS3Container from '@/components/ucilnice/besede/potrebscine/PotrebscineS3Container'
import BesedeKajDelajoVpisovanjeContainer from '@/components/ucilnice/besede/kajDelajo/BesedeKajDelajoVpisovanjeContainer'
import BesedeKajDelajoContainer from '@/components/ucilnice/besede/kajDelajo/BesedeKajDelajoContainer'

import GlagoliS1Container from '@/components/ucilnice/slovnica/glagoli/GlagoliS1Container'
import GlagoliS2Container from '@/components/ucilnice/slovnica/glagoli/GlagoliS2Container'
import GlagoliS3Container from '@/components/ucilnice/slovnica/glagoli/GlagoliS3Container'
import TableSpoliContainer from '@/components/ucilnice/slovnica/table-spoli/TableSpoliContainer'
import TablePovezovanjeContainer from '@/components/ucilnice/slovnica/table/TablePovezovanjeContainer'
import TablePovezovanje2Container from '@/components/ucilnice/slovnica/table/TablePovezovanje2Container'
import TableVpisovanjeContainer from '@/components/ucilnice/slovnica/table/TableVpisovanjeContainer'

import SlovenciIntroContainer from '@/components/ucilnice/slovenci/SlovenciIntroContainer'
import KdoJeToContainer from '@/components/ucilnice/slovenci/KdoJeToContainer'
import KajDelajoVpisovanjeContainer from '@/components/ucilnice/slovenci/KajDelajoVpisovanjeContainer'

import JedilnicaContainer from '@/components/jedilnica/jedilnica/JedilnicaContainer'
import KosiloPovezovanjeContainer from '@/components/jedilnica/jedilnica/kosilo/KosiloPovezovanjeContainer'
import MalicaPovezovanjeContainer from '@/components/jedilnica/jedilnica/malica/MalicaPovezovanjeContainer'
import SadjePovezovanjeContainer from '@/components/jedilnica/jedilnica/sadje/SadjePovezovanjeContainer'
import ZelenjavaPovezovanjeContainer from '@/components/jedilnica/jedilnica/zelenjava/ZelenjavaPovezovanjeContainer'

import KuhinjaContainer from '@/components/jedilnica/kuhinja/KuhinjaContainer'
import JedilniPripomockiPovezovanjeContainer from '@/components/jedilnica/kuhinja/pripomocki/JedilniPripomockiPovezovanjeContainer'
import JedilniPripomockiVpisovanjeContainer from '@/components/jedilnica/kuhinja/pripomocki/JedilniPripomockiVpisovanjeContainer'

import KosareIzbiranjeContainer from '@/components/jedilnica/kosare/kosare/KosareIzbiranjeContainer'
import SadnaKupaSestaviContainer from '@/components/jedilnica/kosare/sadnaKupa/SadnaKupaSestaviContainer'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '*', component: NotFoundContainer },
    {
      path: '/',
      name: 'NaslovnaStran',
      component: IntroTitleContainer
    },
    {
      path: '/uvod',
      name: 'UvodnaAnimacija',
      component: IntroAnimationContainer
    },
    {
      path: '/avla',
      name: 'Avla',
      component: AvlaContainer
    },
    {
      path: '/ucilnice',
      name: 'Ucilnice',
      component: UcilniceContainer
    },
    {
      path: '/ucilnice/besede',
      redirect: { name: 'BesedeUvod' },
      name: 'Besede',
      component: GameContainer,
      children: [
        {
          path: 'uvod',
          name: 'BesedeUvod',
          component: BesedeIntroContainer
        },
        {
          path: 'kaj-je-to-1',
          name: 'KajJeTo-1',
          component: KajJeToContainer
        },
        {
          path: 'kaj-je-to-2',
          name: 'KajJeTo-2',
          component: KajJeToContainer
        },
        {
          path: 'potrebscine-2',
          name: 'Potrebscine-2',
          component: PotrebscineS2Container
        },
        {
          path: 'potrebscine-3',
          name: 'Potrebscine-3',
          component: PotrebscineS3Container
        },
        {
          path: 'kaj-je-to-vpisovanje',
          name: 'KajJeToVpisovanje',
          component: KajJeToVpisovanjeContainer
        },
        {
          path: 'kaj-delajo',
          name: 'BesedeKajDelajo',
          component: BesedeKajDelajoContainer
        },
        {
          path: 'kaj-delajo-vpisovanje',
          name: 'BesedeKajDelajoVpisovanje',
          component: BesedeKajDelajoVpisovanjeContainer
        }
      ]
    },
    {
      path: '/ucilnice/slovnica',
      redirect: { name: 'TableSpoli-1' },
      name: 'Slovnica',
      component: GameContainer,
      children: [
        {
          path: 'table-spoli-1',
          name: 'TableSpoli-1',
          component: TableSpoliContainer
        },
        {
          path: 'table-spoli-2',
          name: 'TableSpoli-2',
          component: TableSpoliContainer
        },
        {
          path: 'glagoli-1',
          name: 'Glagoli-1',
          component: GlagoliS1Container
        },
        {
          path: 'glagoli-2',
          name: 'Glagoli-2',
          component: GlagoliS2Container
        },
        {
          path: 'glagoli-3',
          name: 'Glagoli-3',
          component: GlagoliS3Container
        },
        {
          path: 'table-povezovanje',
          name: 'TablePovezovanje',
          component: TablePovezovanjeContainer
        },
        {
          path: 'table-povezovanje-2',
          name: 'TablePovezovanje-2',
          component: TablePovezovanje2Container
        },
        {
          path: 'table-vpisovanje',
          name: 'TableVpisovanje',
          component: TableVpisovanjeContainer
        }
      ]
    },
    {
      path: '/ucilnice/slovenci',
      redirect: { name: 'KdoJeTo' },
      name: 'slovenci',
      component: GameContainer,
      children: [
        {
          path: 'uvod',
          name: 'SlovenciUvod',
          component: SlovenciIntroContainer
        },
        {
          path: 'kdo-je-to',
          name: 'KdoJeTo',
          component: KdoJeToContainer
        },
        {
          path: 'kaj-delajo-vpisovanje',
          name: 'KajDelajoVpisovanje',
          component: KajDelajoVpisovanjeContainer
        }
      ]
    },
    {
      path: '/jedilnica',
      name: 'Jedilnica',
      component: JedilnicaContainer
    },
    {
      path: '/jedilnica/kosilo',
      redirect: { name: 'KosiloPovezovanje' },
      name: 'kosilo',
      component: GameContainer,
      children: [
        {
          path: 'kosilo-povezovanje',
          name: 'KosiloPovezovanje',
          component: KosiloPovezovanjeContainer
        }
      ]
    },
    {
      path: '/jedilnica/malica',
      redirect: { name: 'MalicaPovezovanje' },
      name: 'malica',
      component: GameContainer,
      children: [
        {
          path: 'malica-povezovanje',
          name: 'MalicaPovezovanje',
          component: MalicaPovezovanjeContainer
        }
      ]
    },
    {
      path: '/jedilnica/sadje',
      redirect: { name: 'SadjePovezovanje' },
      name: 'sadje',
      component: GameContainer,
      children: [
        {
          path: 'sadje-povezovanje',
          name: 'SadjePovezovanje',
          component: SadjePovezovanjeContainer
        }
      ]
    },
    {
      path: '/jedilnica/zelenjava',
      redirect: { name: 'ZelenjavaPovezovanje' },
      name: 'zelenjava',
      component: GameContainer,
      children: [
        {
          path: 'zelenjava-povezovanje',
          name: 'ZelenjavaPovezovanje',
          component: ZelenjavaPovezovanjeContainer
        }
      ]
    },

    {
      path: '/kuhinja',
      name: 'Kuhinja',
      component: KuhinjaContainer
    },
    {
      path: '/kuhinja/pripomocki',
      redirect: { name: 'JedilniPripomockiPovezovanje' },
      name: 'Pripomocki',
      component: GameContainer,
      children: [
        {
          path: 'jedilni-pripomocki-povezovanje',
          name: 'JedilniPripomockiPovezovanje',
          component: JedilniPripomockiPovezovanjeContainer
        },
        {
          path: 'jedilni-pripomocki-vpisovanje',
          name: 'JedilniPripomockiVpisovanje',
          component: JedilniPripomockiVpisovanjeContainer
        }
      ]
    },

    {
      path: '/kosare',
      redirect: { name: 'KosareIzbiranje' },
      name: 'Kosare',
      component: GameContainer,
      children: [
        {
          path: 'kosare-izbiranje',
          name: 'KosareIzbiranje',
          component: KosareIzbiranjeContainer
        },
        {
          path: 'sadna-kupa-sestavi',
          name: 'SadnaKupaSestavi',
          component: SadnaKupaSestaviContainer
        }
      ]
    }
  ]
})
