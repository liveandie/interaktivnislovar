let filters = {
  uppercase: (value) => {
    return value.toUpperCase()
  }
}

export default filters
