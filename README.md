# Interaktivni Slovar

## Okolje za razvoj - Windows

Da lahko poženete testno različico igre potrebujete:

- Node [Link do inštalacije](https://nodejs.org/en/)
- Git [Link do inštalacije](https://git-scm.com/downloads) 

Git bo poleg knjižnice inštaliral tudi Bash. Če nimate nič boljšega kot Cmd in ne znate uporabljati PowerShell, potem priporočam, da vse nadaljne operacije zaganjate v Bash-u.

## Postavitev projekta

Ko imate inštaliran Node ter Git, odprite sebi ljubo konzolo (navodila bodo pisana za Bash) ter sledite navodilom:

1. V konzoli se sprehodite do direktorija v katerem želite imeti datoteke projekta. 
2. Poženite naslednjo komando `git clone https://liveandie@bitbucket.org/liveandie/interaktivnislovar.git`.
3. Ko se podatki preneseje zaženite `cd interaktivnislovar`
4. Znotraj direktorija `/interaktivnislovar` zaženite komando `npm install`
5. Ko se vse inštalira (zna trajati kako minuto ali 5 :)) zaženite `npm run start` 
6. Če je šlo vse po sreči bi se moral odpreti brskalnik in v njem zavihek z igro :).
7. Za zaustavitev programa pritisnite `ctrl+c` v konzoli.

## Posodobitev projekta

Če želite zagnati najnovejšo različico (po tem, ko ste že uspešno zagnali projekt), storite naslednje:

1. V konzoli se postavite na `root` direktorij projekta `/interaktivnislovar`.
2. Zaženite komando `git pull origin master`.
3. Ko se podatki preneseje zaženite `npm install`.
4. Za tem poženite `npm run start`.
7. Za zaustavitev programa pritisnite `ctrl+c` (fokus mora biti na konzoli).

## Priprava za prenos na strežnik
Na sistemu morata biti inštalirana Node ter Git. Potem pa sledite navodilom:

1. V konzoli se sprehodite do direktorija v katerem želite imeti datoteke projekta. 
2. Poženite naslednjo komando `git clone https://liveandie@bitbucket.org/liveandie/interaktivnislovar.git`.
3. Ko se podatki preneseje zaženite `cd interaktivnislovar`
4. Znotraj direktorija `/interaktivnislovar` zaženite komando `npm install`
5. Ko se vse inštalira zaženite `npm run build`
6. Ko se build konča, ce bo v direktoriju `dist` pojavilo vse potrebno za poganjanje igre na strežniku.
7. Celotno vsebino direktorija `dist` prekopirajte na strežnik.
8. To je to, če je strežnik pravilno postavljen, bi se igra morala zagnati na predvidenem URL-ju.
